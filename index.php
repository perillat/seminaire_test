﻿<!DOCTYPE html>
<html lang="en">


  <head>
	<?php
    $TITLE = "Séminaire Phimeca | Appréhender la grande dimension";
    $AUTHOR = "Phimeca";
    $DESCRIPTION = "Les « Rencontres chercheurs et ingénieurs » de Phimeca visent à stimuler la créativité des chercheurs et des ingénieurs pour la résolution de problèmes mettant en jeu la simulation numérique et l’analyse de données, et à encourager les transferts de savoirs et de technologies entre communautés scientifiques.";
	?>
    <?php include('includes/head.php'); ?>
  </head>


<body class="index">
    
    <!-- Off=Canvas Navigation Section -->
	<?php include('includes/navigation.php'); ?>
    
    <!-- Header Section -->
	<?php include('includes/header.php'); ?>
    
    <!-- About Us Section -->
	<?php include('includes/about-us.php'); ?>
    
	<!-- Fun Facts Section -->
	<?php include('includes/funfacts.php'); ?>
	
    <!-- Latest News Section -->
	<?php //include('includes/news.php'); ?>
	
    <!-- Client Section -->
	<?php //include('includes/clients.php'); ?>
	
    <!-- Call to Action Section -->
	<?php include('includes/call.php'); ?>
	
    <!-- About Us Section 2 -->
	<?php include('includes/charts.php'); ?>
    
    <!-- Portfolio Section -->
	<?php // include('includes/portfolio.php'); ?>
    
    <!-- Portfolio Modal Section -->
	<?php i// nclude('includes/portfolio-modal.php'); ?> 

    <!-- Service Section -->
	<?php // include('includes/services.php'); ?>

    <!-- Contact Us Section -->
	<?php include('includes/contact.php'); ?>

    <!-- Team Member Section -->
	<?php //include('includes/team.php'); ?>
	

    <!-- Map Section -->
    <?php // include('includes/google-map.php'); ?>

    <!-- Footer Section -->
	<?php include('includes/footer.php'); ?>
	
	<!-- Footer packager -->
	<?php include('includes/foot.php'); ?>
    
    
</body>
</html>
